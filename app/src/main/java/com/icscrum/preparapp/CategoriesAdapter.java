package com.icscrum.preparapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.icscrum.preparapp.Models.Categorie;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder>
    implements View.OnClickListener{

    private ArrayList<Categorie> categories;
    private ArrayList<CheckBox> checkBoxes;
    private View.OnClickListener listener;

    public CategoriesAdapter(ArrayList<Categorie> categories) {
        this.categories=categories;
        checkBoxes = new ArrayList<>();
    }

    public ArrayList<Categorie> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Categorie> categories) {
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.categorie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesAdapter.ViewHolder holder, final int position) {
        /*
            Esta clase se encarga de mostrar la lista de categorias con un checkbox y devolver la
             lista de recetas con los cambios realizados por el usuario en el caso de que este
             presione el boton "Aceptar"
         */
        Categorie c = categories.get(position);
        holder.cb_categorie.setText(c.getName());
        checkBoxes.add(holder.cb_categorie);
        holder.cb_categorie.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                categories.get(position).setSelected(b);
            }
        });
        /*
        if(c.getSelected()){
            holder.cb_categorie.setChecked(true);
        }
         */
        holder.cb_categorie.setChecked(c.getSelected());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    public void setCheckbox(int position){
        checkBoxes.get(position).setChecked(!checkBoxes.get(position).isChecked());
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            listener.onClick(view);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox cb_categorie;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cb_categorie=itemView.findViewById(R.id.cb_categorie);
        }
    }
}

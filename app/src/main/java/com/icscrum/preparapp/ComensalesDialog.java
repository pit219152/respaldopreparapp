package com.icscrum.preparapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ComensalesDialog extends AppCompatDialogFragment {

    private Button btn_aceptdi, btn_backdi;
    private EditText et_comensalesdi;
    public OnInputSelected onInputSelected;

    public interface OnInputSelected{
        void sendInput(int multiplicador);
    }

    public ComensalesDialog() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_diners,null);
        builder.setView(view);

        et_comensalesdi = view.findViewById(R.id.et_comensalesdi);
        btn_aceptdi = view.findViewById(R.id.btn_aceptdi);
        btn_backdi = view.findViewById(R.id.btn_backdi);

        btn_aceptdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = et_comensalesdi.getText().toString();
                if(!num.isEmpty()){
                    onInputSelected.sendInput(Integer.parseInt(num));
                    getDialog().dismiss();
                }else Toast.makeText(getContext(), "Ingrese un valor", Toast.LENGTH_SHORT).show();
            }
        });

        btn_backdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onInputSelected = (ComensalesDialog.OnInputSelected) context;
        }catch (ClassCastException e){
            Log.e(TAG,"onAttach: ClassCastException: "+e.getMessage());
        }
    }
}

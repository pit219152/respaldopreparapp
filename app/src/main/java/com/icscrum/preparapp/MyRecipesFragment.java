package com.icscrum.preparapp;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyRecipesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyRecipesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String MYRECIPES = "misrecetas";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView rv_myownrecipes, rv_savedrecipes;
    private Button btn_crearRecipe;
    private CatalogueListAdapter misrecetasAdpter, createdrecetasAdapter;

    private ArrayList<Receta> misrecetas, createdRecetas;
    public MyRecipesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyRecipesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyRecipesFragment newInstance(String param1, String param2) {
        MyRecipesFragment fragment = new MyRecipesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_recipes, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.menu_my_recipes);
        //------------------------------------------------------------------------------------------
        btn_crearRecipe = view.findViewById(R.id.btn_crearRecipe);
        rv_myownrecipes = view.findViewById(R.id.rv_myownrecipes);
        rv_savedrecipes = view.findViewById(R.id.rv_savedrecipes);
        //------------------------------------------------------------------------------------------
        ((MainActivity)getActivity()).loadMyRecipes();
        //------------------------------------------------------------------------------------------
        btn_crearRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).displayCreateRecipeFragment();
            }
        });
        //------------------------------------------------------------------------------------------
        return view;
    }

    public void mountMyRecipes(){
        if(misrecetas != null){
            misrecetasAdpter = new CatalogueListAdapter(getContext());
            misrecetasAdpter.agregarRecetas(misrecetas);
            GridLayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(),3);
            rv_savedrecipes.setLayoutManager(manager);
            rv_savedrecipes.setAdapter(misrecetasAdpter);
            //------------------------------------------------------------------------------------------
            // Escuchador OnClik de RecyclerView(lista de recetas)
            misrecetasAdpter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<Receta> recetas = misrecetasAdpter.getRecetas();
                    Receta selected = recetas.get(rv_savedrecipes.getChildAdapterPosition(v));
                    ImageView image = misrecetasAdpter.getImages().get(rv_savedrecipes.getChildAdapterPosition(v));
                    ((MainActivity)getActivity()).displayRecipeInfoFragment(selected,((BitmapDrawable)image.getDrawable()).getBitmap());
                }
            });
        }
    }

    public void mountCreatedRecipes(){
        if(createdRecetas != null){
            createdrecetasAdapter = new CatalogueListAdapter(getContext());
            createdrecetasAdapter.agregarRecetas(createdRecetas);
            GridLayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(),3);
            rv_myownrecipes.setLayoutManager(manager);
            rv_myownrecipes.setAdapter(createdrecetasAdapter);
            //------------------------------------------------------------------------------------------
            // Escuchador OnClik de RecyclerView(lista de recetas)
            createdrecetasAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<Receta> recetas = createdrecetasAdapter.getRecetas();
                    Receta selected = recetas.get(rv_myownrecipes.getChildAdapterPosition(v));
                    ImageView image = createdrecetasAdapter.getImages().get(rv_myownrecipes.getChildAdapterPosition(v));
                    ((MainActivity)getActivity()).displayRecipeInfoFragment(selected,((BitmapDrawable)image.getDrawable()).getBitmap());
                }
            });
        }
    }

    public void setMisrecetas(ArrayList<Receta> misrecetas) {
        this.misrecetas = misrecetas;
    }

    public void setCreatedRecetas(ArrayList<Receta> createdRecetas) {
        this.createdRecetas = createdRecetas;
        mountMyRecipes();
        mountCreatedRecipes();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).setSelectedFragment(MYRECIPES);
        ((MainActivity)getActivity()).loadMyRecipes();
    }
}

package com.icscrum.preparapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ProcessListAdapter extends RecyclerView.Adapter<ProcessListAdapter.ViewHolder> {
    private ArrayList<String> pasos;

    public ProcessListAdapter(ArrayList<String> pasos) {
        this.pasos = pasos;
    }

    @NonNull
    @Override
    public ProcessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.process_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProcessListAdapter.ViewHolder holder, int position) {
        String paso = pasos.get(position);
        int pos = position+1;
        holder.tv_processnum.setText(""+pos);
        holder.tv_processdesc.setText(paso);
    }

    public void setPasos(ArrayList<String> pasos) {
        this.pasos = pasos;
    }

    @Override
    public int getItemCount() {
        return pasos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_processnum, tv_processdesc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_processnum   = itemView.findViewById(R.id.tv_processnum);
            tv_processdesc  = itemView.findViewById(R.id.tv_processdesc);
        }
    }
}

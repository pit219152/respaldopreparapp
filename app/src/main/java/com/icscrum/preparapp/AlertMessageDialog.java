package com.icscrum.preparapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class AlertMessageDialog extends AppCompatDialogFragment implements View.OnClickListener {
    private Button btn_aceptar, btn_cancelar;
    private TextView tv_titulo, tv_mensaje;
    private String titulo, mensaje;
    private static final String TTL = "Actualizar Perfil";

    public OnDialogAnsswer onDialogAnsswer;

    public interface OnDialogAnsswer{
        void sendAndsswer(boolean ansswer);

        void sendAndsswerTo(boolean ansswer);
    }

    public AlertMessageDialog(String sTitulo, String sMensaje) {
        this.titulo = sTitulo;
        this.mensaje = sMensaje;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_alert_message,null);
        builder.setView(view);
        //------------------------------------------------------------------------------------------
        tv_titulo = view.findViewById(R.id.tv_dialogTitle);
        tv_mensaje = view.findViewById(R.id.tv_dialogMessage);
        btn_aceptar = view.findViewById(R.id.btn_aceptar);
        btn_cancelar = view.findViewById(R.id.btn_cancelar);
        //------------------------------------------------------------------------------------------
        tv_titulo.setText(titulo);
        tv_mensaje.setText(mensaje);
        //------------------------------------------------------------------------------------------
        btn_aceptar.setOnClickListener(this);
        btn_cancelar.setOnClickListener(this);
        //------------------------------------------------------------------------------------------
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_aceptar){
            if(titulo.equals(TTL)){
                onDialogAnsswer.sendAndsswerTo(true);
            }else onDialogAnsswer.sendAndsswer(true);
        }else{
            if(titulo.equals(TTL)){
                onDialogAnsswer.sendAndsswerTo(false);
            }else onDialogAnsswer.sendAndsswer(false);
        }
        getDialog().dismiss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        onDialogAnsswer = (OnDialogAnsswer) context;
    }
}

package com.icscrum.preparapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

public class LogActivity extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener{


    private LoginFragment fLoginFragment;
    private ButtonsFragment fButtonsFragment;
    private SignupFragment fSignupFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        setTitle(R.string.app_name);
        fLoginFragment = new LoginFragment();
        fButtonsFragment = new ButtonsFragment();
        fSignupFragment = new SignupFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.logcontainer,fButtonsFragment,null).addToBackStack(null).commit();
    }

    public void userLogged(String regLog){
        Intent intent = new Intent(LogActivity.this, MainActivity.class);
        intent.putExtra("status",regLog);
        startActivity(intent);
        finish();
    }

    public void changeLog(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.logcontainer,fLoginFragment,null).addToBackStack(null).commit();
    }

    public void changeReg(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.logcontainer,fSignupFragment,null).addToBackStack(null).commit();
    }

    public void loadImage(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent,10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Uri path = data.getData();
            fSignupFragment.setImagePath(path);
        }
    }
}

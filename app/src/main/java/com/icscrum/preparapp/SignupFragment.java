package com.icscrum.preparapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SignupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignupFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String SIGNED = "signed";
    private static final String DEFAULTUPDATE = "dupdate";
    private static final String DEFAULT = "default";
    private static final String USERS = "usuarios";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btn_aceptreg;
    private EditText et_emailreg, et_namereg, et_password, et_conpass;
    private ImageView img_reg;
    private Boolean seted = false;
    private FirebaseUser user;
    private FirebaseAuth auth;
    private String imageAc;
    private ProgressDialog progressDialog;
    //----------------------------------------------------------------------------------------------
    private ConnectivityManager connectivityManager;
    private NetworkInfo nwork;

    public SignupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignupFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignupFragment newInstance(String param1, String param2) {
        SignupFragment fragment = new SignupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        //------------------------------------------------------------------------------------------
        btn_aceptreg = view.findViewById(R.id.btn_aceptreg);
        img_reg = view.findViewById(R.id.img_reg);
        img_reg.setDrawingCacheEnabled(true);
        progressDialog = new ProgressDialog(getContext());
        et_namereg = view.findViewById(R.id.et_namereg);
        et_emailreg = view.findViewById(R.id.et_emailreg);
        et_password = view.findViewById(R.id.et_passwordreg);
        et_conpass = view.findViewById(R.id.et_confpasswordreg);
        //------------------------------------------------------------------------------------------
        btn_aceptreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpUser();
            }
        });
        //------------------------------------------------------------------------------------------
        img_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LogActivity)getActivity()).loadImage();
            }
        });
        //------------------------------------------------------------------------------------------
        return view;
    }

    private void signUpUser(){
        if(isNetAbaliable()){
            final String name;
            String  email, password, confpassword;
            name = et_namereg.getText().toString();
            email= et_emailreg.getText().toString().trim();
            password = et_password.getText().toString().trim();
            confpassword = et_conpass.getText().toString().trim();

            if(name.isEmpty()){
                Toast.makeText(getContext(),"Se debe ingresar un nombre",Toast.LENGTH_SHORT).show();
                return;
            }
            if(email.isEmpty()){
                Toast.makeText(getContext(),"Se debe ingresar un Email",Toast.LENGTH_SHORT).show();
                return;
            }
            if(validatepass(password,confpassword)){
                Toast.makeText(getContext(),"Contraseña inválida, debe tener al menos 8 caracteres 1 número",Toast.LENGTH_SHORT).show();
                return;
            }

            auth = FirebaseAuth.getInstance();
            progressDialog.setMessage("Registrando Usuario");
            progressDialog.show();
            auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        updateUser(name);
                    }else{
                        progressDialog.dismiss();
                        if(task.getException() instanceof FirebaseAuthUserCollisionException){
                            Toast.makeText(getContext(),"Ya existe un usuario con este correo",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getContext(),"Error en registro",Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
        }else{
            Toast.makeText(getContext(),"No hay conexion a Internet",Toast.LENGTH_LONG).show();
        }
    }

    private void updateUser(final String name) {
        // Imagen si y solo si el usuario selecciono imagen
        final String rename = name;
        user = auth.getCurrentUser();
        if(seted){
            // MOdificador
            imageAc = "IMG001.jpg";
        }
        else imageAc=DEFAULT;
        UserProfileChangeRequest change = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .setPhotoUri(Uri.parse(imageAc))
                .build();

        user.updateProfile(change).addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    if(seted) uploadImage(imageAc);
                    else{
                        progressDialog.dismiss();
                        ((LogActivity)getActivity()).userLogged(DEFAULTUPDATE);
                    }
                }else{
                    updateUser(rename);
                }
            }
        });
    }

    public void uploadImage(String image){
        StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(USERS+"/"+user.getUid()+"/"+image);
        img_reg.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable)img_reg.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = usersRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(getContext(),"Error cargando imagen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //Toast.makeText(getContext(),"Imagen subida con exito",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                ((LogActivity)getActivity()).userLogged(SIGNED);
            }
        });
    }

    private boolean validatepass(String password, String confpassword) {
        int ch = 0, num = 0;
        for(int i = 0; i<password.length();i++){
            if(password.charAt(i)>='A'&&password.charAt(i)<='Z'||password.charAt(i)>='a'&&password.charAt(i)<='z') ch++;
            if(password.charAt(i)>='0'&&password.charAt(i)<='9') num++;
            if(password.charAt(i)>=' ') return false;
        }
        if(ch>7&&num>0&&password.equals(confpassword)) return true;
        return false;
    }

    public void setImagePath(Uri path){
        img_reg.setImageURI(path);
        seted = true;
    }

    private boolean isNetAbaliable(){
        // Validacion de Estado de Internet
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        nwork= connectivityManager.getActiveNetworkInfo();
        return (nwork != null && nwork.isConnected());
    }

}

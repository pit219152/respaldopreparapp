package com.icscrum.preparapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.icscrum.preparapp.DataBase.ImagesLoader;
import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;

public class CatalogueListAdapter extends RecyclerView.Adapter<CatalogueListAdapter.ViewHolder>
        implements View.OnClickListener{
    /*
        Esta Clase se encarga de formatear los datos de recetas en una lista, que esa formada de
        elementos "item_catalogue_list" se comunica con ImagesLoader para cargar as imagenes y
         recibe la lista de recetas del Fragmento padre CatalogueFragment
     */

    private ArrayList<Receta> recetas = null;
    private ImagesLoader imagesLoader;
    private Context context;
    private ArrayList<ImageView> images;
    private  View.OnClickListener listener;

    public ArrayList<Receta> getRecetas() {
        return recetas;
    }

    public void setRecetas(ArrayList<Receta> recetas ) {
        this.recetas = recetas;
    }

    public CatalogueListAdapter(Context c) {
        context=c;
        recetas = new ArrayList<>();
        imagesLoader = new ImagesLoader();
        images=new ArrayList<>();
        this.recetas = new ArrayList<Receta>();
    }

    @NonNull
    @Override
    public CatalogueListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Aqui va el listener
        View contextView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cataloguelists, parent, false);
        contextView.setOnClickListener(this);
        return new ViewHolder(contextView);

    }

    public ArrayList<ImageView> getImages() {
        return images;
    }

    @Override
    public void onBindViewHolder(@NonNull CatalogueListAdapter.ViewHolder holder, int position) {
        Receta lReceta = recetas.get(position);
        holder.img_Card.setImageResource(R.drawable.dish);
        imagesLoader.mountImage(holder.img_Card,context,lReceta.getImagen());
        holder.tv_CardName.setText(lReceta.getNombre());
        images.add(holder.img_Card);
    }

    @Override
    public int getItemCount() {
        return recetas.size();
    }

    public void agregarRecetas(ArrayList<Receta> nRecetas){
        recetas.addAll(nRecetas);
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_CardName;
        private ImageView img_Card;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //Aqui se realiza el enlace de los elementos visuales dde cada item
            tv_CardName=itemView.findViewById(R.id.tv_CardName);
            img_Card=itemView.findViewById(R.id.img_Card);
        }
    }
}

package com.icscrum.preparapp;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.DataBase.ImagesLoader;
import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecipeInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecipeInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TITLESAVE = "Guardar Receta";
    private static final String TITLEDELETE = "Borrar Receta";
    private static final String TITLECREATED = "Borrar Receta Publicada";
    private static final String MESSAGESAVE = "Esta seguro de querer guardar la receta: \n";
    private static final String MESSAGEDELETE = "Esta seguro de querer borrar la receta guardada: \n";
    private static final String MESSAGECREATED = "Esta seguro de querer borrar SU receta PUBLICADA, esta operacion es irreversible, para confirmar ingrese: \n";
    private static final String IMAGENES = "imagenes";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    //----------------------------------------------------------------------------------------------
    //VARS PREPARAPP
    private TextView tv_recipename, tv_recipeauthor, tv_revipediners, tv_fuente;
    private ImageView img_recipe;
    private RecyclerView lv_ingredients, lv_process;
    private Button btn_rediners, btn_saveRecipe;
    private IngredientListAdapter ingAdapter;
    private ProcessListAdapter proAdapter;
    private Receta receta;
    private Bitmap image;
    private ImagesLoader imgLoader;
    private boolean saved = false;
    private boolean created = false;

    //----------------------------------------------------------------------------------------------
    public RecipeInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecipeInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecipeInfoFragment newInstance(String param1, String param2) {
        RecipeInfoFragment fragment = new RecipeInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_info, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.txt_recipeinfo);
        //------------------------------------------------------------------------------------------
        // Vinculacion
        tv_recipename = view.findViewById(R.id.tv_recipename);
        tv_recipeauthor = view.findViewById(R.id.tv_recipeauthor);
        tv_revipediners = view.findViewById(R.id.tv_recipediners);
        tv_fuente = view.findViewById(R.id.tv_fuente2);

        lv_process = view.findViewById(R.id.lv_process);
        lv_ingredients = view.findViewById(R.id.lv_ingredients);

        btn_rediners = view.findViewById(R.id.btn_rediners);
        btn_saveRecipe = view.findViewById(R.id.btn_saveRecipe);
        img_recipe= view.findViewById(R.id.img_recipe);
        imgLoader = new ImagesLoader();
        //------------------------------------------------------------------------------------------
        // Montaje datos receta
        if(receta!=null){
            mountRecipe();
            created = ((MainActivity)getActivity()).isCreated(receta);
            if(!created){
                saved = ((MainActivity)getActivity()).isSaved(receta);
            }
            setButtonType();
            btn_rediners.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getActivity()).displayDialog();
                }
            });
        }
        //------------------------------------------------------------------------------------------
        return view;
    }

    public void calculateIngredients(float  multiplicador){
        ArrayList<String> ncantidades = new ArrayList<String>();
        float diners = Float.valueOf(receta.getComensales());
        for(int i=0;i<receta.getCantidades().size();i++){
            ncantidades.add(separarCantidad(receta.getCantidades().get(i),diners, multiplicador));
        }
        ingAdapter.setCantidades(ncantidades);
        lv_ingredients.setAdapter(ingAdapter);
        tv_revipediners.setText(""+multiplicador);
    }


    private void mountRecipe() {
        // TV
        tv_recipename.setText(receta.getNombre());
        tv_recipeauthor.setText(receta.getAutor());
        tv_revipediners.setText(receta.getComensales());
        if(receta.getFuente()!=null){
            tv_fuente.setText(receta.getFuente());
        }
        //----------------Imagen--------------------------------------------------------------------
        //img_recipe.setImageBitmap(image);
        imgLoader.mountImageCache(img_recipe,getContext(),receta.getImagen());
        //----------------Pasos---------------------------------------------------------------------
        ArrayList<String> pasos = getPasos(receta.getDescripcion());
        //----------------Adaptadores---------------------------------------------------------------
        ingAdapter = new IngredientListAdapter(receta.getIngredientes(),receta.getCantidades());
        proAdapter = new ProcessListAdapter(pasos);
        LinearLayoutManager manager1 = new LinearLayoutManager(getContext());
        LinearLayoutManager manager2 = new LinearLayoutManager(getContext());
        lv_ingredients.setAdapter(ingAdapter);
        lv_ingredients.setLayoutManager(manager1);
        lv_process.setAdapter(proAdapter);
        lv_process.setLayoutManager(manager2);
    }

    private String separarCantidad(String cantidad, float din, float mul){
        char c;
        float ncanat;
        String cant = "";
        for(int i=0;i<cantidad.length();i++){
            c=cantidad.charAt(i);
            if((c>='A'&&c<='Z')||(c>='a'&&c<='z')){
                cant=cantidad.substring(0,i);
                ncanat=Float.valueOf(cant);
                ncanat=ncanat/din*mul;
                return String.valueOf(ncanat)+cantidad.substring(i);
            }
        }
        ncanat = Float.valueOf(cantidad);
        ncanat=ncanat/din*mul;
        return String.valueOf(ncanat);
    }

    private ArrayList<String> getPasos(String descripcion) {
        String[] splits = descripcion.split("\\.");
        return new ArrayList<String>(Arrays.asList(splits));
    }

    public void saveRecipeMethod(Boolean ansswer){
        if(ansswer){
            FireBaseTalker fireBaseTalker= new FireBaseTalker();
            fireBaseTalker.setRecipeInfoFragment(RecipeInfoFragment.this);
            if(!saved){
                fireBaseTalker.uploadMyRecipe(receta, FirebaseAuth.getInstance().getCurrentUser().getUid());
            }else{
                String id = ((MainActivity)getActivity()).idsavedR(receta);
                fireBaseTalker.deleteSavedRecipe(id, FirebaseAuth.getInstance().getCurrentUser().getUid());
            }
        }
    }

    public void confirmSaved(boolean confirm){
        if(confirm) {
            if (saved)
                Toast.makeText(getContext(), "Se elimino la receta", Toast.LENGTH_SHORT).show();
            else Toast.makeText(getContext(), "Se guardo Exitosamente", Toast.LENGTH_SHORT).show();
            saved = !saved;
            setButtonType();
        }else{
            if (saved)
                Toast.makeText(getContext(), "Error al eliminar...", Toast.LENGTH_SHORT).show();
            else Toast.makeText(getContext(), "Error al guardar...", Toast.LENGTH_SHORT).show();
        }
    }

    public void confirmDel(boolean confirm){
        if(confirm){
            if(created){
                created = false;
                eliminarImagen(receta.getImagen());
            }
        }else{
            Toast.makeText(getContext(), "Error al eliminar...", Toast.LENGTH_SHORT).show();
        }

    }

    public void deletePubr(String s){
        // ver si receta carga utomaticamente hermana en created
        if(s.equals(receta.getNombre())){
            FireBaseTalker fireBaseTalker= new FireBaseTalker();
            fireBaseTalker.setRecipeInfoFragment(RecipeInfoFragment.this);
            String id = ((MainActivity)getActivity()).idcreatedR(receta);
            fireBaseTalker.deleteCreatedRecipe(id, FirebaseAuth.getInstance().getCurrentUser().getUid());
            fireBaseTalker.deleteHermanaRecipe(receta.getHermana());
        }
    }

    public void setButtonType(){
        if(!created){
            if(saved){
                btn_saveRecipe.setText(getText(R.string.txt_del_receta));
                btn_saveRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity)getActivity()).displayAlertDialog(TITLEDELETE, MESSAGEDELETE+receta.getNombre());
                    }
                });
            }else{
                btn_saveRecipe.setText(getText(R.string.txt_save_receta));
                btn_saveRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity)getActivity()).displayAlertDialog(TITLESAVE, MESSAGESAVE+receta.getNombre());
                    }
                });
            }
        }else{
            btn_saveRecipe.setText(getText(R.string.txt_del_receta));
            if(receta.getHermana()!= null){
                btn_saveRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity)getActivity()).displayAtenticationDialog(TITLECREATED,MESSAGECREATED+receta.getNombre());
                    }
                });
            }else{
                btn_saveRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(),"Para borrar esta receta, dirijase a Mis recetas",Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }

    }

    private void eliminarImagen(String imagen){
        StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(IMAGENES+"/"+imagen);
        usersRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                eliminarImagen(receta.getImagen());
            }
        });
    }

    public void setReceta(Receta receta) {
        this.receta = receta;
    }
    public void setImage(Bitmap image) {
        this.image = image;
    }
    public void setImgLoader(ImagesLoader loader){
        this.imgLoader = loader;
    }
}

package com.icscrum.preparapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.icscrum.preparapp.Models.Categorie;

import java.util.ArrayList;

public class CatalogueDialog extends AppCompatDialogFragment {

    /*
    Esa clase conecta al Didalogo Emergente que muestra las categorias en pantalla
     */
    private Button btn_dialogAcept;
    private RecyclerView rv_checkbox;
    private ArrayList<Categorie> categories;
    private static final String TAG = "DIALOG: ";
    public interface OnIputSelected{
        void sendInput(ArrayList<Categorie> input);
    }

    public CatalogueDialog(ArrayList<Categorie> categories) {
        this.categories=new ArrayList<>(categories);
    }
    public OnIputSelected onIputSelected;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.catalogue_dialog,null);
        builder.setView(view);

        rv_checkbox = view.findViewById(R.id.rv_checkbox);
        final CategoriesAdapter adapter = new CategoriesAdapter(categories);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rv_checkbox.setLayoutManager(manager);
        rv_checkbox.setAdapter(adapter);
        btn_dialogAcept=view.findViewById(R.id.btn_dialogAcept);
        
        btn_dialogAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                onIputSelected.sendInput(categories);
            }
        });

        return builder.create();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onIputSelected= (OnIputSelected) context;
        }catch (ClassCastException e){
            Log.e(TAG,"onAttach: ClassCastException: "+e.getMessage());
        }
    }

}

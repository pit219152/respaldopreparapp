package com.icscrum.preparapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Preparapp_Splashscreen extends Activity {
    /*
        Muestra una vista por dos segundos del logo de la aplicacion, cada vez que esta se inicia
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparapp__splashscreen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Preparapp_Splashscreen.this, LogActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}

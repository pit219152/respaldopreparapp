package com.icscrum.preparapp;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.Models.Categorie;
import com.icscrum.preparapp.Models.Receta;

import java.io.ByteArrayOutputStream;
import java.text.Normalizer;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateRecipeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateRecipeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static final String IMAGENES = "imagenes";

    private EditText et_nombreRecipec, et_comensalesRecipec, et_pasoRecipec, et_ingredienteRecipec, et_cantidadRecipec, et_fuenteRecipec;
    private CheckBox cb_fuenteRecipec;
    private ImageButton btn_addpasoRecipec;
    private ImageView img_addimgRecipec;
    private RecyclerView rv_ingredientesRecipec, rv_pasosRecipec, rv_categoriasRecipec;
    private Button btn_addingredienteRecipec, btn_crear;

    private Receta r;

    private CategoriesAdapter categoriesAdapter;
    private ProcessListAdapter processListAdapter;
    private IngredientListAdapter ingredientListAdapter;

    private FireBaseTalker fDataBase;

    private ArrayList<String> ingredientes, cantidades, pasos;
    private ArrayList<Categorie> categorias;
    private Uri path;

    private ProgressDialog progressDialog;
    // VLIDAR PUNTOS
    // no inredienttes sin cantidad ni al reves


    public CreateRecipeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateRecipeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateRecipeFragment newInstance(String param1, String param2) {
        CreateRecipeFragment fragment = new CreateRecipeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_recipe, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.txt_crear_receta);
        //------------------------------------------------------------------------------------------
        et_nombreRecipec = view.findViewById(R.id.et_nombreRecipec);
        et_comensalesRecipec = view.findViewById(R.id.et_comensalesRecipec);
        et_pasoRecipec = view.findViewById(R.id.et_pasoRecipec);
        et_ingredienteRecipec = view.findViewById(R.id.et_ingredienteRecipec);
        et_cantidadRecipec = view.findViewById(R.id.et_cantidadRecipec);
        et_fuenteRecipec = view.findViewById(R.id.et_fuenteRecipec);
        cb_fuenteRecipec = view.findViewById(R.id.cb_fuenteRecipec);
        btn_addpasoRecipec = view.findViewById(R.id.btn_addpasoRecipec);
        img_addimgRecipec = view.findViewById(R.id.img_addimgRecipec);
        rv_ingredientesRecipec = view.findViewById(R.id.rv_ingredientesRecipec);
        rv_pasosRecipec = view.findViewById(R.id.rv_pasosRecipec);
        rv_categoriasRecipec = view.findViewById(R.id.rv_categoriasRecipec);
        btn_addingredienteRecipec = view.findViewById(R.id.btn_addingredienteRecipec);
        btn_crear = view.findViewById(R.id.btn_crear);
        //------------------------------------------------------------------------------------------
        et_fuenteRecipec.setEnabled(false);
        ingredientes = new ArrayList<>();
        cantidades = new ArrayList<>();
        pasos = new ArrayList<>();
        categorias = new ArrayList<>();
        path = null;
        progressDialog = new ProgressDialog(getContext());
        //------------------------------------------------------------------------------------------
        LinearLayoutManager managerp = new LinearLayoutManager(getContext());
        rv_pasosRecipec.setLayoutManager(managerp);
        LinearLayoutManager manageri = new LinearLayoutManager(getContext());
        rv_ingredientesRecipec.setLayoutManager(manageri);
        GridLayoutManager managerc = new GridLayoutManager(getContext(),2);
        rv_categoriasRecipec.setLayoutManager(managerc);
        //------------------------------------------------------------------------------------------
        //ADAPTADORES
        ingredientListAdapter = new IngredientListAdapter(ingredientes,cantidades);
        processListAdapter = new ProcessListAdapter(pasos);
        categoriesAdapter = new CategoriesAdapter(categorias);
        //------------------------------------------------------------------------------------------
        // Inicializacion de FireBasetalker (BD)
        fDataBase = new FireBaseTalker();
        fDataBase.setCreateRecipeFragment(CreateRecipeFragment.this);
        //Consulttar Categorias
        fDataBase.loadCategoriesCreate();
        //------------------------------------------------------------------------------------------
        //Listener Pasos
        btn_addpasoRecipec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarPaso(validarPunto(et_pasoRecipec.getText().toString()));
                et_pasoRecipec.setText("");
            }
        });

        //------------------------------------------------------------------------------------------
        cb_fuenteRecipec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_fuenteRecipec.setEnabled(cb_fuenteRecipec.isChecked());
                if(cb_fuenteRecipec.isChecked()) Toast.makeText(getContext(),"Ingrese la fuente",Toast.LENGTH_SHORT).show();
            }
        });
        //------------------------------------------------------------------------------------------
        btn_addingredienteRecipec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarIngrediene(
                validarTexto(et_ingredienteRecipec.getText().toString().trim()),
                validarCantidad(et_cantidadRecipec.getText().toString()).trim());
                et_ingredienteRecipec.setText("");
                et_cantidadRecipec.setText("");
            }
        });
        //------------------------------------------------------------------------------------------
        img_addimgRecipec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).loadImg();
            }
        });
        //------------------------------------------------------------------------------------------
        btn_crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearReceta();
            }
        });
        //------------------------------------------------------------------------------------------
        return view;
    }

    private String validarPunto(String cadena){
        if(!cadena.isEmpty()){
            for (int i=0;i<cadena.length();i++){
                if(cadena.charAt(i)=='.'){
                    agregarPaso(validarPunto(cadena.substring(i+1)));
                    return (cadena.substring(0,i));
                }
            }
            return cadena;
        }else Toast.makeText(getContext(), "NO se puede agregar pasos vacios", Toast.LENGTH_SHORT).show();
        return null;
    }

    private boolean validarNumeros(String cadena){
        if(!cadena.isEmpty()){
            for (int i=0;i<cadena.length();i++){
                if(!(cadena.charAt(i)>='0'&&cadena.charAt(i)<='9')){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void agregarPaso(String paso){
        //caadena y adaptador DEScripcion al final
        // puede que el crear nuevos adaaptaadores sea lo que altera ls vistas
        if (paso!=null){
            pasos.add(paso);
            processListAdapter.setPasos(pasos);
            rv_pasosRecipec.setAdapter(processListAdapter);
        }else Toast.makeText(getContext(), "Paso no valido", Toast.LENGTH_SHORT).show();
    }

    private String validarTexto(String txt){
        String novalido = "0123456789_,.;:+*-/!¡?¿|&%$#@°'(){}[]<>= ";
        if(!txt.isEmpty()){
            char c;
            for(int i=0;i<txt.length();i++) {
                if (novalido.contains(String.valueOf(txt.charAt(i)))){
                        return null;
                }
            }
            String a = "";
            a = txt.substring(0,1).toUpperCase();
            a = a+txt.substring(1).toLowerCase();
            return a;
        }
        return null;
    }

    private String validarNombre(String txt){
        String novalido = "0123456789_,.;:+*-/!¡?¿|&%$#@°'(){}[]<>=";
        if(!txt.isEmpty()){
            char c;
            for(int i=0;i<txt.length();i++) {
                if (novalido.contains(String.valueOf(txt.charAt(i)))){
                    return null;
                }
            }
            return txt.toUpperCase();
        }
        return null;
    }

    private String validarCantidad(String txt){
        if(!txt.isEmpty()){
            String txt2 = "";
            int num = 0;
            for(int i=0;i<txt.length();i++){
                if(txt.charAt(i)>='0'&&txt.charAt(i)<='9'){
                    num++;
                }
                if(txt.charAt(i)!=' '){
                    txt2 = txt2 +txt.charAt(i);
                }
            }
            if(num >0 && txt2.length()>=1) return txt2;
        }
        return null;
    }

    private void agregarIngrediene(String ing, String cant){
        if(ing == null){
            Toast.makeText(getContext(), "Ingrediente no valido", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cant == null){
            Toast.makeText(getContext(), "Cantidad no valida", Toast.LENGTH_SHORT).show();
            return;
        }
        ingredientes.add(ing);
        cantidades.add(cant);
        ingredientListAdapter.setIngredientes(ingredientes);
        ingredientListAdapter.setCantidades(cantidades);
        rv_ingredientesRecipec.setAdapter(ingredientListAdapter);
    }

    public void onCategoriesLoad(ArrayList<Categorie> categorias){
        this.categorias = categorias;
        categoriesAdapter.setCategories(categorias);
        rv_categoriasRecipec.setAdapter(categoriesAdapter);
    }

    private void crearReceta(){
        r = new Receta();
        // quitar proceso de punto
        // Cocnseguir id de Hermana y generarla Hermana
        // Al borrar, borrar receta y hermana
        String nombre = validarNombre(et_nombreRecipec.getText().toString());
        String comensales = et_comensalesRecipec.getText().toString();
        String descripcion = generarDescripcion();
        String autor = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        String fuente = et_fuenteRecipec.getText().toString();
        ArrayList selectedcategories =  getSelectedItems(categoriesAdapter.getCategories());
        if(nombre == null){
            Toast.makeText(getContext(), "Se requiere del Nombre", Toast.LENGTH_SHORT).show();
            return;
        }
        if(comensales.isEmpty()){
            Toast.makeText(getContext(), "Comensales no Validos", Toast.LENGTH_SHORT).show();
            return;
        }
        if(descripcion.isEmpty()){
            Toast.makeText(getContext(), "Debe agregar al menos un paso", Toast.LENGTH_SHORT).show();
            return;
        }
        if(ingredientes.size()==0){
            Toast.makeText(getContext(), "Debe agregar al menos un Ingrediente", Toast.LENGTH_SHORT).show();
            return;
        }
        if(selectedcategories.size()==0){
            Toast.makeText(getContext(), "Debe agregar al menos una Categoria", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cb_fuenteRecipec.isChecked() && fuente.isEmpty()){
            Toast.makeText(getContext(), "Debe agregar la fuente", Toast.LENGTH_SHORT).show();
            return;
        }
        if(path==null){
            Toast.makeText(getContext(), "Debe agregar una Imagen", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Publicando receta...");
        progressDialog.show();
        r.setAutor(autor);
        r.setNombre(nombre);
        r.setComensales(comensales);
        r.setDescripcion(descripcion);
        r.setIngredientes(ingredientes);
        r.setCantidades(cantidades);
        r.setCategorias(selectedcategories);
        r.setFuente(fuente);
        r.setImagen(parseImagen(nombre+autor));

        //------------------------------------------------------------------------------------------
        // RELIZAR VALIDACION DE SI EXISTe
        if(((MainActivity)getActivity()).isCreated(r)){
            Toast.makeText(getContext(),"Ya tiene una receta con el nombre "+ r.getNombre(),Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }else fDataBase.uploadHermana(r, FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    private ArrayList<String> getSelectedItems(ArrayList<Categorie> cItems){
        ArrayList<String> items = new ArrayList<>();
        for(int i=0;i<cItems.size();i++){
            if(cItems.get(i).getSelected()) items.add(cItems.get(i).getName());
        }
        return items;
    }

    public void setImagePath(Uri path){
        this.path = path;
        img_addimgRecipec.setImageURI(path);
    }

    public String parseImagen(String m){
        String img="";
        String n = Normalizer.normalize(m, Normalizer.Form.NFD);
        n = n.replaceAll("[\\p{InCOMBINING_DIACRITICAL_MARKS}]","");
        char c;
        for(int i=0;i<n.length();i++){
            c=n.charAt(i);
            if(c==' '){
                img = img+"_";
            }else{
                img = img + String.valueOf(c);
            }
        }
        return img+".jpg";
    }

    private String generarDescripcion(){
        String s = "";
        if(pasos.size()>0){
            s = ""+pasos.get(0);
            for(int i= 1;i<pasos.size();i++){
                s = s + "." + pasos.get(i);
            }
        }
        return s;
    }

    public void confirmSaved(boolean conf){
        // UN mensaje y cerrar la vista
        // Validar si la receta ya existe pideiendo isCreated??
        if(conf){
            guardarImagen();
        } else Toast.makeText(getContext(),"Error Publicando Receta", Toast.LENGTH_SHORT).show();
    }

    public void confirmHermana(String hermana){
        // UN mensaje y cerrar la vista
        // Validar si la receta ya existe pideiendo isCreated??
        if(!hermana.isEmpty()){
            r.setHermana(hermana);
            fDataBase.uploadCreatedRecipe(r, FirebaseAuth.getInstance().getCurrentUser().getUid());
        } else Toast.makeText(getContext(),"Error Publicando Receta", Toast.LENGTH_SHORT).show();
    }

    private void guardarImagen(){
        StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(IMAGENES+"/"+r.getImagen());
        img_addimgRecipec.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable)img_addimgRecipec.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = usersRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(),"Error cargando imagen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getContext(),"Receta publicada con exito",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                ((MainActivity)getActivity()).displayMyRecipesFragment();
            }
        });
    }
}

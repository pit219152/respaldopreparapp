package com.icscrum.preparapp;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.provider.MediaStore;
import android.view.View;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.Models.Categorie;
import com.icscrum.preparapp.Models.Receta;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.Menu;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        CatalogueFragment.OnFragmentInteractionListener,
        ComensalesDialog.OnInputSelected,
        AlertMessageDialog.OnDialogAnsswer,
        AutenticationDialog.OnEntry{

    // Variables Necesarias para transicion
    //FRAGMENTOS
    private CatalogueFragment fCatalogueFragment;
    private SearchFragment fSearchFragment;
    private FiltredRecipesFragment fFiltredRecipesFragment;
    private RecipeInfoFragment fRecipeInfoFragment;
    private AccountFragment fAccountFragment;
    private MyRecipesFragment fMyRecipesFragment;
    private CreateRecipeFragment fCreateRecipeFragment;
    private EditPerfilFragment fEditPerfilFragment;
    private FireBaseTalker ftalker;
    private AboutFragment fAboutFragment;
    // CONSTANTESS
    public static final String CATALOGUE = "catalogo";
    public static final String SRCHRECIPES = "searchrecipes";
    public static final String CREARRECETA = "crearreceta";
    public static final String FILTRECIPES = "filtredrecipes";
    public static final String MYRECIPES = "misrecetas";
    public static final String RECIPEINFO = "recipeinfo";
    public static final String ACCOUNT = "account";
    public static final String ABOUT = "about";
    public static final String EDITPERFIL = "Editar Perfil";
    public static final String DEFAULTUPDATE = "dupdate";

    private String selectedFragment;
    private  ArrayList<Receta> misRecetas, createdRecetas;

    private MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //CODIGO POR DEFECTO
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        /*
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        //------------------------------------------------------------------------------------------
        // EXTRAS
        //------------------------------------------------------------------------------------------
        //CODIGO PREPARAPP
        ftalker = new FireBaseTalker();
        ftalker.setMainActivity(MainActivity.this);
        fCatalogueFragment      = new CatalogueFragment();
        fSearchFragment         = new SearchFragment();
        fFiltredRecipesFragment = new FiltredRecipesFragment();
        fRecipeInfoFragment     = new RecipeInfoFragment();
        fAccountFragment        = new AccountFragment();
        fMyRecipesFragment      = new MyRecipesFragment();
        fCreateRecipeFragment   = new CreateRecipeFragment();
        fEditPerfilFragment     = new EditPerfilFragment();
        fAboutFragment          = new AboutFragment();
        //------------------------------------------------------------------------------------------
        selectedFragment=CATALOGUE;
        loadMyRecipes();
        //------------------------------------------------------------------------------------------
        fCatalogueFragment.setFloating(fab);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.lycontainer, fCatalogueFragment, null).addToBackStack(null).commit();
        setTitle(R.string.menu_search_catalogue);
        navigationView.setCheckedItem(R.id.nav_search_catalogue);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        /*
            Aquí se realiza la la transición de los fragmentos usando el listener del navigationbar
            por el momento esta inhabilitada.
         */
        int id = item.getItemId();

        if (id == R.id.nav_search_catalogue&&selectedFragment!=CATALOGUE) {
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.lycontainer, fCatalogueFragment).commit();
            //setTitle(R.string.menu_search_catalogue);
            selectedFragment = CATALOGUE;
        } else if (id == R.id.nav_my_recipes&&selectedFragment!=MYRECIPES) {
            fMyRecipesFragment.setMisrecetas(misRecetas);
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.lycontainer, fMyRecipesFragment).commit();
            //setTitle(R.string.menu_my_recipes);
            selectedFragment = MYRECIPES;
        } else if (id == R.id.nav_account&&selectedFragment!=ACCOUNT) {
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.lycontainer, fAccountFragment).commit();
            selectedFragment = ACCOUNT;

        } else if (id == R.id.nav_about&&selectedFragment!=ABOUT) {
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.lycontainer, fAboutFragment).commit();
            selectedFragment = ABOUT;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void displayMyRecipesFragment(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.lycontainer, fMyRecipesFragment).commit();
        selectedFragment = MYRECIPES;
    }

    public void displaySearchFragment(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.lycontainer, fSearchFragment).addToBackStack(null).commit();
        //setTitle(R.string.txt_search_recipes);
        selectedFragment = SRCHRECIPES;
    }

    public void displayFiltredRecipesFragment(String name, String diners, ArrayList cat, ArrayList ing){

        fFiltredRecipesFragment.setSearchParameters(name,diners,cat,ing);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.lycontainer, fFiltredRecipesFragment).addToBackStack(null).commit();
        //setTitle(R.string.txt_filtred_recipes);
        selectedFragment = FILTRECIPES;
    }

    public void displayRecipeInfoFragment(Receta receta, Bitmap bit){
        fRecipeInfoFragment.setReceta(receta);
        fRecipeInfoFragment.setImage(bit);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.lycontainer, fRecipeInfoFragment).addToBackStack(null).commit();
        //setTitle(R.string.txt_recipeinfo);
        selectedFragment = RECIPEINFO;
    }

    public void displayCreateRecipeFragment(){
        getSupportFragmentManager().beginTransaction().
                replace(R.id.lycontainer, fCreateRecipeFragment).addToBackStack(null).commit();
        //setTitle(R.string.txt_crear_receta);
        selectedFragment = CREARRECETA;
    }

    public void displayEditPerfilFragment(){
        getSupportFragmentManager().beginTransaction().
                replace(R.id.lycontainer, fEditPerfilFragment).addToBackStack(null).commit();
        //setTitle(EDITPERFIL);
        selectedFragment = EDITPERFIL;
    }

    public void backToLog(){
        Intent intent = new Intent(MainActivity.this, LogActivity.class);
        startActivity(intent);
        finish();
    }


    public void displayDialog(){
        ComensalesDialog dialogo = new ComensalesDialog();
        dialogo.show(getSupportFragmentManager(),null);
    }

    public void displayAlertDialog(String titulo, String mensaje){
        AlertMessageDialog alertMessageDialog = new AlertMessageDialog(titulo,mensaje);
        alertMessageDialog.show(getSupportFragmentManager(),null);
    }

    public void displayAtenticationDialog(String titulo, String mensaje){
        AutenticationDialog autenticationDialog = new AutenticationDialog(titulo, mensaje);
        autenticationDialog.show(getSupportFragmentManager(),null);
    }

    @Override
    public void sendInput(int multiplicador) {
        fRecipeInfoFragment.calculateIngredients(multiplicador);
    }

    public boolean isSaved(Receta receta){
        for(int i=0;i<misRecetas.size();i++){
            if(misRecetas.get(i).getNombre().equals(receta.getNombre()) &&
                    misRecetas.get(i).getAutor().equals(receta.getAutor())){
                return  true;
            }
        }
        return false;
    }

    public String idsavedR(Receta receta){
        for(int i=0;i<misRecetas.size();i++){
            if(misRecetas.get(i).getNombre().equals(receta.getNombre()) &&
                    misRecetas.get(i).getAutor().equals(receta.getAutor())){
                return  misRecetas.get(i).getId();
            }
        }
        return null;
    }

    public boolean isCreated(Receta receta){
        for(int i=0;i<createdRecetas.size();i++){
            if(createdRecetas.get(i).getNombre().equals(receta.getNombre()) &&
                    createdRecetas.get(i).getAutor().equals(receta.getAutor())){
                return  true;
            }
        }
        return false;
    }

    public String idcreatedR(Receta receta){
        for(int i=0;i<createdRecetas.size();i++){
            if(createdRecetas.get(i).getNombre().equals(receta.getNombre()) &&
                    createdRecetas.get(i).getAutor().equals(receta.getAutor())){
                return  createdRecetas.get(i).getId();
            }
        }
        return null;
    }

    public void loadMyRecipes(){
        ftalker.loadMyRecipes(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    public void onLoadMyRecipes(ArrayList<Receta> misRecetas){
        this.misRecetas = misRecetas;
        if(selectedFragment == MYRECIPES||selectedFragment == CREARRECETA){
            fMyRecipesFragment.setMisrecetas(misRecetas);
        }
        ftalker.loadCreatedRecipes(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }
    public void onLoadCreatedRecipes(ArrayList<Receta> createdRecetas){
        this.createdRecetas = createdRecetas;
        if(selectedFragment == MYRECIPES||selectedFragment == CREARRECETA){
            fMyRecipesFragment.setCreatedRecetas(createdRecetas);
        }
    }

    public void loadImg(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent,10);
    }

    public void loadUSeRImg(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent,8);
    }

    @Override
    public void sendAndsswer(boolean ansswer) {
        fRecipeInfoFragment.saveRecipeMethod(ansswer);
        if(ansswer)loadMyRecipes();
    }

    @Override
    public void sendAndsswerTo(boolean ansswer) {
        fEditPerfilFragment.getAnsswer(ansswer);
    }

    public ArrayList<Receta> getMisRecetas() {
        return misRecetas;
    }

    public ArrayList<Receta> getCreatedRecetas() {
        return createdRecetas;
    }

    @Override
    public void sendEntry(String entry) {
        fEditPerfilFragment.giveEntry(entry);
    }

    @Override
    public void sendCEntry(String entry) {
        fRecipeInfoFragment.deletePubr(entry);
    }

    public void setSelectedFragment(String selectedFragment) {
        this.selectedFragment = selectedFragment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Uri path = data.getData();
            if(requestCode==10){
                fCreateRecipeFragment.setImagePath(path);
            }else{
                fEditPerfilFragment.setImagePath(path);
            }


        }
    }
}

package com.icscrum.preparapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class AutenticationDialog extends AppCompatDialogFragment implements View.OnClickListener {

    private TextView tv_titulo, tv_mensaje;
    private EditText et_entrada;
    private Button btn_aceptar, btn_cancelar;
    private String mensaje, titulo;
    private static final String TITLECREATED = "Borrar Receta Publicada";

    public OnEntry onEntry;

    public interface OnEntry{
        void sendEntry(String entry);

        void sendCEntry(String entry);
    }

    public AutenticationDialog(String titulo, String mensaje) {
        this.mensaje = mensaje;
        this.titulo = titulo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_autentication,null);
        builder.setView(view);
        //------------------------------------------------------------------------------------------
        tv_titulo = view.findViewById(R.id.tv_titleAut);
        tv_mensaje = view.findViewById(R.id.tv_messageaut);
        btn_aceptar = view.findViewById(R.id.btn_aceptaraut);
        btn_cancelar = view.findViewById(R.id.btn_cancelaraut);
        et_entrada = view.findViewById(R.id.et_entryaut);
        //------------------------------------------------------------------------------------------
        tv_titulo.setText(titulo);
        tv_mensaje.setText(mensaje);
        //------------------------------------------------------------------------------------------
        btn_aceptar.setOnClickListener(this);
        btn_cancelar.setOnClickListener(this);
        //------------------------------------------------------------------------------------------
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        onEntry = (OnEntry) context;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_aceptaraut){
            if(titulo.equals(TITLECREATED)){
                String entry = et_entrada.getText().toString();
                onEntry.sendCEntry(entry);
            }else {
                String entry = et_entrada.getText().toString();
                onEntry.sendEntry(entry);
            }
        }
        getDialog().dismiss();
    }
}

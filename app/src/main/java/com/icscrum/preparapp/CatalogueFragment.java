package com.icscrum.preparapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.Models.Categorie;
import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link CatalogueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatalogueFragment extends Fragment{
    /*
        Esta clase permite visualizar una lista de Recetas que se carga de forma dinamica,
         utilizando FireBaseTalker para obtener los Datos y ImagesLoader para cargar las imagenes
     */
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    //----------------------------------------------------------------------------------------------
    //VARS PREPARAPP
    private RecyclerView recyclerView;
    private CatalogueListAdapter clAdapter;
    private FireBaseTalker fDataBase;
    private Context context;
    private Button btn_search;
    private Boolean enableLoad;
    private GridLayoutManager manager;
    private ConnectivityManager connectivityManager;
    private NetworkInfo nwork;
    private FloatingActionButton floating;
    //----------------------------------------------------------------------------------------------

    public CatalogueFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatalogueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatalogueFragment newInstance(String param1, String param2) {
        CatalogueFragment fragment = new CatalogueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_catalogue, container, false);
        /*
            Aqui se realiza el enlace de los elementos visuales del recurso fragment-catalogue.xml
            con la clase CatalogueFragment. Se crea los esuchadores para el campo de consulta y
             Scroll para la lista de Recetas
         */
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.menu_search_catalogue);
        //------------------------------------------------------------------------------------------
        // PreparaApp Code - Enlace elementos visuales
        //sv_bar          = v.findViewById(R.id.sv_bar);
        btn_search  = v.findViewById(R.id.btn_search);
        recyclerView    = v.findViewById(R.id.rv_cataloguelist);
        context         = getContext();
        clAdapter       = new CatalogueListAdapter(context);
        //------------------------------------------------------------------------------------------
        // Inicializacion de FireBasetalker (BD)
        fDataBase   = new FireBaseTalker();
        fDataBase.setCatalogueFragment(CatalogueFragment.this);
        //------------------------------------------------------------------------------------------
        // Inicializacion de Banderas de Control
        enableLoad  = false;
        //------------------------------------------------------------------------------------------
        // Configuración de RecyclerView(lista de recetas)
        manager = new GridLayoutManager(getActivity().getApplicationContext(),3);
        recyclerView.setLayoutManager(manager);
        //------------------------------------------------------------------------------------------
        // Escuchador Boton Buscar  Recetas
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).displaySearchFragment();
            }
        });
        //------------------------------------------------------------------------------------------
        // Escuchador onScroll de RecyclerView(lista de recetas)
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Codigo cargador OnScroll
                if(dy>0){
                    int visibleItemCount = manager.getChildCount();
                    int totalItemContent = manager.getItemCount();
                    int pasVisibleItems = manager.findFirstVisibleItemPosition();
                    if(enableLoad){
                        if(visibleItemCount + pasVisibleItems >= totalItemContent){
                            if(isNetAbaliable()){
                                enableLoad = false;
                                loadNextData();
                            }else{
                                floating.setVisibility(View.VISIBLE);
                                Toast.makeText(context,"No hay conexion a Internet",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        });
        //------------------------------------------------------------------------------------------
        // Escuchador OnClik de RecyclerView(lista de recetas)
        clAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Receta> recetas = clAdapter.getRecetas();
                Receta selected = recetas.get(recyclerView.getChildAdapterPosition(v));
                ImageView image = clAdapter.getImages().get(recyclerView.getChildAdapterPosition(v));
                ((MainActivity)getActivity()).displayRecipeInfoFragment(selected,((BitmapDrawable)image.getDrawable()).getBitmap());
            }
        });
        //------------------------------------------------------------------------------------------
        // Cargado recetas
        if(isNetAbaliable()){
            floating.setVisibility(View.INVISIBLE);
            loadData();
        }
        else{
            floating.setVisibility(View.VISIBLE);
            Toast.makeText(context,"No hay conexion a Internet",Toast.LENGTH_LONG).show();
        }
        return v;
    }
    //----------------------------------------------------------------------------------------------
    private boolean isNetAbaliable(){
        // Validacion de Estado de Internet
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        nwork= connectivityManager.getActiveNetworkInfo();
        return (nwork != null && nwork.isConnected());
    }

    private void loadData() {
        fDataBase.loadRecetas();
    }

    private void loadNextData() {
        //Carga 10 Recetas en el RecyclerView, solamente despues de haber ejecutado loadData
        fDataBase.loadNextRecetas();
    }

    public void refreshCatalogue(ArrayList<Receta> nuevasRecetas){
        //Refresca la lista despues de cada solicitud ded carga de recetas, se encarga de hacer
        // visibles las recetas cargadas
        clAdapter.agregarRecetas(nuevasRecetas);
        recyclerView.setAdapter(clAdapter);
        enableLoad=true;
    }

    public void refreshALL(){
        // Reinicia todos los valores a sus estados por defecto
        Toast.makeText(getContext(),"Refrescando...",Toast.LENGTH_SHORT).show();
        if(isNetAbaliable()){
            loadData();
            floating.setVisibility(View.INVISIBLE);
        }else{
            floating.setVisibility(View.VISIBLE);
            Toast.makeText(context,"No hay conexion a Internet",Toast.LENGTH_LONG).show();
        }
    }

    private ArrayList<String> getSelectedCategorie(ArrayList<Categorie> categories){
        // Deevuelve las catergorias que hn sido seleccionadas por el Usuario
        ArrayList<String> sCat = new ArrayList<>();
        for (int i =0;i<categories.size();i++){
            if (categories.get(i).getSelected()){
                sCat.add(categories.get(i).getName());
            }
        }
        return sCat;
    }

    public void setFloating(FloatingActionButton fba){
        // Enlaza el boto flotante con el fragmento
        this.floating=fba;
        floating.setVisibility(View.INVISIBLE);
        floating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshALL();
            }
        });
    }
    //----------------------------------------------------------------------------------------------
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
//------------------------------------------------------------------------------------------
//Escuchador de Campo de Consulta - INHABILITADO
        /*
        sv_bar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(!s.isEmpty()){
                    changeCat=true;
                    onSearch=true;
                    fDataBase.searchRecetas(s);
                    floating.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(getContext(),"El campo esta vacio",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
         */
//------------------------------------------------------------------------------------------
// Validacion de Coneccion a Internet y carga de Categorias - INHABILITADO
        /*
        if(isNetAbaliable()){
            fDB.collection("categorias").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if(task.isSuccessful()){
                        for(QueryDocumentSnapshot document : task.getResult()) {
                            categories.add(new Categorie((String)document.getData().get("nombre"),false));
                        }
                        //------------------------------------------------------------------------------
                        //Codifique Aqui Categorias
                        btn_categories.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((MainActivity)getActivity()).displayDialog(categories);
                            }
                        });

                    }
                }
            });
            loadData();
            floating.setVisibility(View.INVISIBLE);
        }else{
            floating.setVisibility(View.VISIBLE);
            Toast.makeText(context,"No hay conexion a Internet",Toast.LENGTH_LONG).show();
        }*/

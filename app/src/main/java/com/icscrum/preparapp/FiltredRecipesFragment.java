package com.icscrum.preparapp;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.DataBase.InnerFilter;
import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FiltredRecipesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FiltredRecipesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // Elementos Visuales
    private RecyclerView rv_filtredrecipes;
    private CatalogueListAdapter adapter;
    private Button btn_back;
    //Elementos para datos
    private ArrayList<String> categories, ingredients;
    private String name, diners;
    // Elementos de Conexion
    private FireBaseTalker fDataBase;
    private InnerFilter innerFilter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FiltredRecipesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FiltredRecipesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FiltredRecipesFragment newInstance(String param1, String param2) {
        FiltredRecipesFragment fragment = new FiltredRecipesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_filtred_recipes, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.txt_filtred_recipes);
        //------------------------------------------------------------------------------------------
        // Vinculacion
        rv_filtredrecipes = v.findViewById(R.id.rv_filtredrecipes);
        btn_back = v.findViewById(R.id.btn_back);
        adapter = new CatalogueListAdapter(getContext());
        GridLayoutManager manager = new GridLayoutManager(getContext(),3);
        rv_filtredrecipes.setLayoutManager(manager);
        rv_filtredrecipes.setAdapter(adapter);
        //------------------------------------------------------------------------------------------
        // Inicializacion de FireBasetalker (BD)
        fDataBase = new FireBaseTalker();
        fDataBase.setFiltredRecipesFragment(FiltredRecipesFragment.this);
        innerFilter = new InnerFilter(name,diners,categories,ingredients);
        //------------------------------------------------------------------------------------------
        // Listener Boton Volver
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        //------------------------------------------------------------------------------------------
        // Escuchador OnClik de RecyclerView(lista de recetas)
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Receta> recetas = adapter.getRecetas();
                Receta selected = recetas.get(rv_filtredrecipes.getChildAdapterPosition(v));
                ImageView image = adapter.getImages().get(rv_filtredrecipes.getChildAdapterPosition(v));
                ((MainActivity)getActivity()).displayRecipeInfoFragment(selected,((BitmapDrawable)image.getDrawable()).getBitmap());
            }
        });
        //------------------------------------------------------------------------------------------
        // Cargar recetas filtradas
        callLoadFiltredRecipes();
        //------------------------------------------------------------------------------------------
        return v;
    }

    public void setSearchParameters(String name, String diners, ArrayList cat, ArrayList ing){
        this.name = name;
        this.diners=diners;
        this.categories=cat;
        this.ingredients=ing;
    }

    private void callLoadFiltredRecipes() {
        if (categories.size()>0 && !name.isEmpty()) fDataBase.loadRecetasFiltred(name,categories);
        else{
            if(categories.size()>0 && name.isEmpty()) fDataBase.loadRecetasFiltred(categories);
            else {
                if (!name.isEmpty()) fDataBase.loadRecetasFiltred(name);
                else fDataBase.loadRecetasFiltred();
            }
        }
    }

    public void onLoadFiltredRecipes(ArrayList<Receta> recetas){
        ArrayList r;
        r = innerFilter.filterRecetas(recetas);
        if(r.size()>0){
            adapter.agregarRecetas(r);
            rv_filtredrecipes.setAdapter(adapter);
        }else{
            Toast.makeText(getContext(), "No se encontraron recetas", Toast.LENGTH_SHORT).show();
        }
    }
}

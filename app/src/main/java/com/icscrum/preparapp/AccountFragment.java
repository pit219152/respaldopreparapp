package com.icscrum.preparapp;

import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.icscrum.preparapp.DataBase.ImagesLoader;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String DEFAULT = "default";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView img_account;
    private TextView tv_emailaccount,tv_nameaccount;
    private Button btn_logout, btn_editAccoun;

    private FirebaseUser user;
    private Uri path;

    private ImagesLoader loader;

    public AccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance(String param1, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.menu_account);
        //------------------------------------------------------------------------------------------
        tv_nameaccount = view.findViewById(R.id.tv_nameaccount);
        tv_emailaccount = view.findViewById(R.id.tv_emailaccount);
        img_account = view.findViewById(R.id.img_account);
        btn_logout = view.findViewById(R.id.btn_logout);
        btn_editAccoun = view.findViewById(R.id.btn_edit);
        //------------------------------------------------------------------------------------------
        user = FirebaseAuth.getInstance().getCurrentUser();
        loader = new ImagesLoader();
        loadData();
        //------------------------------------------------------------------------------------------
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                ((MainActivity)getActivity()).backToLog();
            }
        });
        //------------------------------------------------------------------------------------------
        btn_editAccoun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).displayEditPerfilFragment();
            }
        });
        //------------------------------------------------------------------------------------------
        return view;
    }

    private void loadData() {
        String image = "";
        if(user!=null){
            if(user.getPhotoUrl()!=null){
                image = user.getPhotoUrl().toString();
                if(!image.equals(DEFAULT)){
                    loader.mountUserImage(img_account,getContext(),user.getUid(),image);
                }else Toast.makeText(getContext(),"Defult",Toast.LENGTH_SHORT).show();
            }
            tv_emailaccount.setText(user.getEmail());
            tv_nameaccount.setText(user.getDisplayName());
        }
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public void setPath(Uri path) {
        this.path = path;
    }
}

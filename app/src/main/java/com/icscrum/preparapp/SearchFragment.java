package com.icscrum.preparapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.Models.Categorie;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    // Elementos Visuales
    private RecyclerView rv_categories, rv_ingredients;
    private EditText et_recipename, et_diners;
    private Button btn_search;
    private CategoriesAdapter adapterCat, adapterIng;
    // Elementos de Conexion
    private FireBaseTalker fDataBase;
    private ConnectivityManager connectivityManager;
    private NetworkInfo nwork;
    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.txt_search_recipes);
        //------------------------------------------------------------------------------------------
        //Enlace de Variables
        rv_categories = v.findViewById(R.id.rv_categories);
        rv_ingredients = v.findViewById(R.id.rv_ingredients);
        et_recipename = v.findViewById(R.id.et_name);
        et_diners = v.findViewById(R.id.et_diners);
        btn_search = v.findViewById(R.id.btn_search_2);
        //------------------------------------------------------------------------------------------
        // Inicializacion de FireBasetalker (BD)
        fDataBase = new FireBaseTalker();
        fDataBase.setSearchFragment(SearchFragment.this);
        //------------------------------------------------------------------------------------------
        // Cargar Categorias
        fDataBase.loadCategories();
        //------------------------------------------------------------------------------------------
        // Cargar Ingredientes
        fDataBase.loadIngredientes();
        //------------------------------------------------------------------------------------------
        // Escuchador boton Buscar
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchRecipes();
            }
        });
        //------------------------------------------------------------------------------------------
        // Escuchador EditText Buscar
        et_recipename.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!validateChar(String.valueOf(et_recipename.getText()).toUpperCase())){
                        Toast.makeText(getContext(),"El nombre de receta solo permite letras y espacios",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        //------------------------------------------------------------------------------------------
        return v;
    }

    public void onLoadCategories(ArrayList<Categorie> categories) {
        adapterCat = new CategoriesAdapter(categories);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rv_categories.setLayoutManager(manager);
        rv_categories.setAdapter(adapterCat);
    }

    public void onLoadIngredients(ArrayList<Categorie> ingredientes){
        adapterIng = new CategoriesAdapter(ingredientes);
        GridLayoutManager manager = new GridLayoutManager(getContext(),2);
        rv_ingredients.setLayoutManager(manager);
        rv_ingredients.setAdapter(adapterIng);
    }

    private ArrayList<String> getSelectedItems(ArrayList<Categorie> cItems){
        ArrayList<String> items = new ArrayList<>();
        for(int i=0;i<cItems.size();i++){
            if(cItems.get(i).getSelected()) items.add(cItems.get(i).getName());
        }
        return items;
    }

    private void searchRecipes(){
        // vALIDAR MAYUS E INTERNET
        if(isNetAbaliable()){
            String name = "", diners = "";
            name = (String.valueOf(et_recipename.getText())).toUpperCase();
            diners = String.valueOf(et_diners.getText());
            ArrayList ingredients =  getSelectedItems(adapterIng.getCategories());
            ArrayList categories =  getSelectedItems(adapterCat.getCategories());
            if(!name.isEmpty() || !diners.isEmpty() ||ingredients.size()>0 || categories.size()>0){
                ((MainActivity)getActivity()).displayFiltredRecipesFragment(name,diners,categories, ingredients);
            }
            else Toast.makeText(getContext(),"Se requiere al menos un parametro de busqueda",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getContext(),"No hay conexion a Internet",Toast.LENGTH_LONG).show();
        }

    }

    private boolean validateChar(String s ){
        if(!s.isEmpty()){
            for (int i=0;i<s.length();i++){
                if(!((s.charAt(i)>='A'&& s.charAt(i)<='Z')||s.charAt(i)==' ')) return false;
            }
        }
        return true;
    }

    private boolean isNetAbaliable(){
        // Validacion de Estado de Internet
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        nwork= connectivityManager.getActiveNetworkInfo();
        return (nwork != null && nwork.isConnected());
    }
}

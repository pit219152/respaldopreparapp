package com.icscrum.preparapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private final  static String LOGED = "loged";
    //----------------------------------------------------------------------------------------------
    private EditText et_email, et_password;
    private Button btn_aceptlog;
    private ProgressDialog progressDialog;
    //----------------------------------------------------------------------------------------------
    private FirebaseAuth firebaseAuth;
    private ConnectivityManager connectivityManager;
    private NetworkInfo nwork;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        //------------------------------------------------------------------------------------------
        et_email = view.findViewById(R.id.et_emaillog);
        et_password = view.findViewById(R.id.et_passwordlog);
        btn_aceptlog = view.findViewById(R.id.btn_aceptlog);
        //------------------------------------------------------------------------------------------
        // Instancia Conexion
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(getContext());
        //------------------------------------------------------------------------------------------
        btn_aceptlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetAbaliable()){
                    logUser();
                }else Toast.makeText(getContext(),"No hay conexion a Internet",Toast.LENGTH_LONG).show();

            }
        });


        return view;
    }

    private void logUser() {
        String email = et_email.getText().toString().trim();
        String password = et_password.getText().toString().trim();

        if(email.isEmpty()) {
            Toast.makeText(getContext(),"Debe ingresar un Email",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.isEmpty()) {
            Toast.makeText(getContext(),"Debe ingresar su contraseña",Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Iniciando Sesión");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    ((LogActivity)getActivity()).userLogged(LOGED);
                }else{
                    Toast.makeText(getContext(),"Datos incorrectos",Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
        });

    }

    private boolean isNetAbaliable(){
        // Validacion de Estado de Internet
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        nwork= connectivityManager.getActiveNetworkInfo();
        return (nwork != null && nwork.isConnected());
    }

    public interface OnFragmentInteractionListener {

    }
    
}
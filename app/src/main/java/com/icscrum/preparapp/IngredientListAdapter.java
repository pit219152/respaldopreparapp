package com.icscrum.preparapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.ViewHolder> {

    private ArrayList<String> ingredientes, cantidades;

    public IngredientListAdapter(ArrayList<String> ingredientes, ArrayList<String> cantidades) {
        this.ingredientes = ingredientes;
        this.cantidades = cantidades;
    }

    public void setIngredientes(ArrayList<String> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public ArrayList<String> getCantidades() {
        return cantidades;
    }

    public void setCantidades(ArrayList<String> cantidades) {
        this.cantidades = cantidades;
    }

    @NonNull
    @Override
    public IngredientListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListAdapter.ViewHolder holder, int position) {
        String name = ingredientes.get(position);
        String cant = cantidades.get(position);

        holder.tv_ingredientname.setText(name);
        holder.tv_ingredientcant.setText(cant);

    }

    @Override
    public int getItemCount() {
        return ingredientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_ingredientname, tv_ingredientcant;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_ingredientname = itemView.findViewById(R.id.tv_ingredientname);
            tv_ingredientcant = itemView.findViewById(R.id.tv_ingredientcant);
        }
    }
}

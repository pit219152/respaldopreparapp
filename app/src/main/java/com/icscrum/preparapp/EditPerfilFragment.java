package com.icscrum.preparapp;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.icscrum.preparapp.DataBase.FireBaseTalker;
import com.icscrum.preparapp.Models.Receta;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditPerfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditPerfilFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String RCREATED = "rcreated";
    private static final String RHERMANA = "rhermana";
    private static final String RSAVED = "rsaved";
    private static final String IMAGENES = "imagenes";
    public static final String RECETAS = "recetas";
    public static final String GRACIAS = "Muchas gracias por usar nuestra App... ";
    public static final String SAVED = "saved";
    public static final String USERS = "usuarios";
    public static final String AUTOR = "autor";
    public static final String MS = "Se actualizaran los campos: ";
    public static final String IMG001 = "IMG001.jpg";
    public static final String IMG002 = "IMG002.jpg";
    public static final String IMG003 = "IMG003.jpg";
    public static final String IMG004 = "IMG004.jpg";
    private static final String TTL = "Actualizar Perfil";
    public static final String CREATED = "created";
    private static final String DEFAULT = "default";
    private ProgressDialog progressDialog;
    private ArrayList<Receta> recetas;
    private ArrayList<Receta> savedrecetas;



    private static final String MENSAJE = "Esta seguro de querer borrar su cuenta, este proceso es irreversible, si desea continuar por favor digite \n";
    private static final String TITULO = "¿¡ELIMINAR CUENTA!?";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText et_nombre, et_password, et_confpass;
    private ImageView imgview;
    private Button btn_save, btn_delete;
    private String email, imagen;
    private FirebaseUser user;
    private FirebaseFirestore fDB;

    private int actual, salida, recetaActual;
    private  String tipoactual, nombre;
    private boolean fnombre, fpassword, fimg;

    public EditPerfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditPerfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditPerfilFragment newInstance(String param1, String param2) {
        EditPerfilFragment fragment = new EditPerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_perfil, container, false);
        //------------------------------------------------------------------------------------------
        // Titulo
        getActivity().setTitle(R.string.txt_editPerfil);
        //------------------------------------------------------------------------------------------
        //ENLACE
        btn_save = view.findViewById(R.id.btn_savedit);
        btn_delete = view.findViewById(R.id.btn_delAccount);

        et_nombre = view.findViewById(R.id.et_nameedit);
        et_password = view.findViewById(R.id.et_passwordedit);
        et_confpass = view.findViewById(R.id.et_confpasswordedit);

        imgview = view.findViewById(R.id.img_edit);
        email = FirebaseAuth.getInstance().getCurrentUser().getEmail();

        progressDialog = new ProgressDialog(getContext());
        //------------------------------------------------------------------------------------------
        fimg = false;
        fnombre = false;
        fpassword = false;
        actual = 0;
        salida = 0;
        recetaActual = 0;
        //------------------------------------------------------------------------------------------
        user = FirebaseAuth.getInstance().getCurrentUser();
        //------------------------------------------------------------------------------------------
        fDB = FirebaseFirestore.getInstance();
        //------------------------------------------------------------------------------------------
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).displayAtenticationDialog(TITULO,MENSAJE+email);
            }
        });
        //------------------------------------------------------------------------------------------
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).loadUSeRImg();
            }
        });
        //------------------------------------------------------------------------------------------
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizar();
            }
        });
        //------------------------------------------------------------------------------------------
        return view;
    }

    public void giveEntry(String entry){
        if(!entry.isEmpty()){
            if (entry.equals(email)) borrarUsuario();
            else Toast.makeText(getContext(),"Entrada Incorrecta", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getContext(),"Cadena Vacia", Toast.LENGTH_SHORT).show();
        }
    }

    private void borrarUsuario(){
        // Verificar Contraseña de usurio
        // Si imagen es default eliminar directamente al usuario
        progressDialog.setMessage("Eliminando perfil...");
        progressDialog.show();
        recetas = ((MainActivity)getActivity()).getCreatedRecetas();
        savedrecetas = ((MainActivity)getActivity()).getMisRecetas();
        //------------------------------------------------------------------------------------------
        // Empezar borrado
        recetaActual = 0;
        if(recetas.size()>0){
            tipoactual = RHERMANA;
            delImg(recetas.get(recetaActual).getImagen());
        }else{
            if(savedrecetas.size()>0){
                tipoactual = RSAVED;
                deleteSavedRecipe(savedrecetas.get(recetaActual).getId(),user.getUid());
            }else{
                Toast.makeText(getContext(),"NO hay recetas que borrar", Toast.LENGTH_SHORT).show();
                //borrar Usuario
                if(!user.getPhotoUrl().toString().equals(DEFAULT)){
                    delUserImg(user.getUid(),user.getPhotoUrl().toString());
                }else {
                    endDelete();
                }
            }
        }
    }

    private void delImg(String imagen){
        StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(IMAGENES+"/"+imagen);
        usersRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if(tipoactual.equals(RHERMANA)){
                    if(recetaActual<recetas.size()-1) {
                        recetaActual++;
                        delImg(recetas.get(recetaActual).getImagen());
                    }else{
                        recetaActual=0;
                        deleteHermanaRecipe(recetas.get(recetaActual).getHermana());
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FIREBASE","Excepcion",e.getCause());
                if(tipoactual.equals(RHERMANA)){
                    if(recetaActual<recetas.size()-1) {
                        recetaActual++;
                        delImg(recetas.get(recetaActual).getImagen());
                    }else{
                        recetaActual=0;
                        deleteHermanaRecipe(recetas.get(recetaActual).getHermana());
                    }
                }
            }
        });
    }
    public void deleteHermanaRecipe(final String receta){
        fDB.collection(RECETAS).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(tipoactual.equals(RHERMANA)){
                            if(recetaActual<recetas.size()-1) {
                                recetaActual++;
                                deleteHermanaRecipe(recetas.get(recetaActual).getHermana());
                            }else{
                                recetaActual=0;
                                tipoactual = RCREATED;
                                deleteCreatedRecipe(recetas.get(recetaActual).getId(),user.getUid());
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FIREBASE","Excepcion",e.getCause());
                if(tipoactual.equals(RHERMANA)){
                    if(recetaActual<recetas.size()-1) {
                        recetaActual++;
                        deleteHermanaRecipe(recetas.get(recetaActual).getHermana());
                    }else{
                        recetaActual=0;
                        tipoactual = RCREATED;
                        deleteCreatedRecipe(recetas.get(recetaActual).getId(),user.getUid());
                    }
                }
            }
        });
    }

    public void deleteCreatedRecipe(String receta, String owner){
        fDB.collection(USERS).document(owner).collection(CREATED).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(tipoactual.equals(RCREATED)){
                            if(recetaActual<recetas.size()-1) {
                                recetaActual++;
                                deleteCreatedRecipe(recetas.get(recetaActual).getId(),user.getUid());
                            }else{
                                recetaActual=0;
                                tipoactual = RSAVED;
                                if(savedrecetas.size()>0){
                                    deleteSavedRecipe(savedrecetas.get(recetaActual).getId(),user.getUid());
                                }else{
                                    if(!user.getPhotoUrl().toString().equals(DEFAULT)){
                                        delUserImg(user.getUid(),user.getPhotoUrl().toString());
                                    }else endDelete();
                                }
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //inctError();
                Log.e("FIREBASE","Excepcion",e.getCause());
                if(tipoactual.equals(RCREATED)){
                    if(recetaActual<recetas.size()-1) {
                        recetaActual++;
                        deleteCreatedRecipe(recetas.get(recetaActual).getId(),user.getUid());
                    }else{
                        recetaActual=0;
                        tipoactual = RSAVED;
                        if(savedrecetas.size()>0){
                            deleteSavedRecipe(savedrecetas.get(recetaActual).getId(),user.getUid());
                        }else{
                            if(!user.getPhotoUrl().toString().equals(DEFAULT)){
                                delUserImg(user.getUid(),user.getPhotoUrl().toString());
                            }else endDelete();
                        }
                    }
                }
            }
        });
    }



    public void deleteSavedRecipe(String receta, String owner){
        fDB.collection(USERS).document(owner).collection(SAVED).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(tipoactual.equals(RSAVED)){
                            if(recetaActual<savedrecetas.size()-1) {
                                recetaActual++;
                                deleteSavedRecipe(savedrecetas.get(recetaActual).getId(),user.getUid());
                            }else{
                                delUserImg(user.getUid(),user.getPhotoUrl().toString());
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //inctError();
                Log.e("FIREBASE","Excepcion",e.getCause());
                if(tipoactual.equals(RSAVED)){
                    if(recetaActual<savedrecetas.size()-1) {
                        recetaActual++;
                        deleteSavedRecipe(savedrecetas.get(recetaActual).getId(),user.getUid());
                    }else{
                        if(!user.getPhotoUrl().toString().equals(DEFAULT)){
                            delUserImg(user.getUid(),user.getPhotoUrl().toString());
                        }else endDelete();
                    }
                }
            }
        });
    }

    private void delUserImg(String user, String imagen){
        if(!user.equals(DEFAULT)){
            StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(USERS+"/"+user+"/"+imagen);
            usersRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    endDelete();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FIREBASE","Excepcion",e.getCause());
                    //inctError();
                    endDelete();
                }
            });
        }
    }

    private void endDelete(){
        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    progressDialog.dismiss();
                    Toast.makeText(getContext(),GRACIAS,Toast.LENGTH_LONG).show();
                    FirebaseAuth.getInstance().signOut();
                    ((MainActivity)getActivity()).backToLog();
                }else{
                    Log.e("FIREBASE","Excepcion Usuario");
                    progressDialog.dismiss();
                    Toast.makeText(getContext(),"Error borrando usuario",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    //----------------------------------------------------------------------------------------------
    private void actualizar(){
        fnombre = !et_nombre.getText().toString().isEmpty();
        fpassword = !et_password.getText().toString().isEmpty();
        nombre = null;
        String mensaje = ""+MS;
        if(fnombre){
            nombre = validarNombre(et_nombre.getText().toString());
            if(nombre==null){
                Toast.makeText(getContext(),"El nuevo nombre no es valido", Toast.LENGTH_SHORT).show();
                return;
            }
            salida++;
            mensaje=mensaje+"\n Nombre de Usuario";
        }
        if(fpassword){
           String pass =  et_password.getText().toString().trim();
           String confpass =  et_confpass.getText().toString().trim();
           if(!validarPass(pass)){
               Toast.makeText(getContext(),"La nueva contraseña debe tener al menos 8 caracteres y dos número", Toast.LENGTH_SHORT).show();
               et_password.setText("");
               et_confpass.setText("");
               return;
           }
           if(!pass.equals(confpass)){
               et_password.setText("");
               et_confpass.setText("");
               Toast.makeText(getContext(),"La nueva contraseña no coincide", Toast.LENGTH_SHORT).show();
               return;
           }
           salida++;
            mensaje=mensaje+"\n Contraseña";
        }
        if(fimg){
            Toast.makeText(getContext(),"Esta operación puede tomar algunos minutos", Toast.LENGTH_SHORT).show();
            salida++;
            mensaje=mensaje+"\n Imagen";
        }
        if(fpassword||fnombre||fimg){
            ((MainActivity)getActivity()).displayAlertDialog(TTL,mensaje);
        }else{
            Toast.makeText(getContext(),"NO se han realizado cambios", Toast.LENGTH_SHORT).show();
        }


    }

    public void setImagePath(Uri path){
        fimg = true;
        imgview.setImageURI(path);
    }

    public void getAnsswer(boolean ans){
        if(ans){
            progressDialog.setMessage("Actualizando...");
            progressDialog.show();
            if(fnombre){
                UserProfileChangeRequest change;
                change = new UserProfileChangeRequest.Builder()
                            .setDisplayName(nombre).build();

                user.updateProfile(change).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        recetas = ((MainActivity)getActivity()).getCreatedRecetas();
                        recetaActual = 0;
                        tipoactual = RHERMANA;
                        if(recetas.size()>0){
                            actualizarCreatedRecipe(recetas.get(recetaActual).getHermana());
                        }else{
                            inctActual();
                        }
                    }
                });
            }
            if(fpassword){
                user.updatePassword(et_password.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(getContext(),"Error Actualizando contraseña", Toast.LENGTH_SHORT).show();
                        }else{
                            inctActual();
                        }
                    }
                });
            }
            if(fimg){
                imagen = user.getPhotoUrl().toString();
                UserProfileChangeRequest change = new UserProfileChangeRequest.Builder()
                        .setPhotoUri(Uri.parse(nuevaImagen(imagen))).build();
                user.updateProfile(change).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        delimgUpdate(imagen, user.getUid());
                    }
                });
            }
        }
        /*
        et_confpass.setText("");
            et_password.setText("");
            et_nombre.setText("");
            fimg = false;
         */
    }

    private String nuevaImagen(String imagen){
        switch (imagen){
            case IMG001:
                return IMG002;
            case IMG002:
                return IMG003;
            case IMG003:
                return IMG004;
            case IMG004:
                return IMG001;
            default:
                return IMG001;
        }
    }

    private void actualizarCreatedRecipe(final String receta){
        if(tipoactual==RHERMANA){
            fDB.collection(RECETAS).document(receta)
                    .update(AUTOR, nombre)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if(tipoactual==RHERMANA){
                                if(recetaActual<recetas.size()-1){
                                    recetaActual++;
                                    actualizarCreatedRecipe(recetas.get(recetaActual).getHermana());
                                }else{
                                    recetaActual=0;
                                    tipoactual=RCREATED;
                                    actualizarCreatedRecipe(recetas.get(recetaActual).getId());
                                }
                            }
                        }
                    });
        }else{
            fDB.collection(USERS).document(user.getUid()).collection(CREATED).document(receta)
                    .update(AUTOR, nombre)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if(recetaActual<recetas.size()-1){
                                recetaActual++;
                                actualizarCreatedRecipe(recetas.get(recetaActual).getId());
                            }else{
                                inctActual();
                            }
                        }
                    });
        }
    }

    private boolean validarPass(String password) {
        int ch = 0, num = 0;
        for(int i = 0; i<password.length();i++){
            if(password.charAt(i)>='A'&&password.charAt(i)<='Z'||password.charAt(i)>='a'&&password.charAt(i)<='z') ch++;
            if(password.charAt(i)>='0'&&password.charAt(i)<='9') num++;
            if(password.charAt(i)==' ') return false;
        }
        if(ch>7&&num>0) return true;
        return false;
    }

    public void inctActual() {
        this.actual++;
        if (actual >= salida){
            progressDialog.dismiss();
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(getContext(),"Actualización exitosa",Toast.LENGTH_SHORT).show();
            ((MainActivity)getActivity()).backToLog();
        }
    }

    private String validarNombre(String txt){
        String novalido = "0123456789_,.;:+*-/!¡?¿|&%$#@°'(){}[]<>=";
        if(!txt.isEmpty()){
            char c;
            for(int i=0;i<txt.length();i++) {
                if (novalido.contains(String.valueOf(txt.charAt(i)))){
                    return null;
                }
            }
            return txt.toUpperCase();
        }
        return null;
    }

    private void delimgUpdate(String s, String u){
        if(!s.equals(DEFAULT)){
            StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(USERS+"/"+u+"/"+s);
            usersRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateImg(nuevaImagen(imagen),user.getUid());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    updateImg(nuevaImagen(imagen),user.getUid());
                }
            });
        }else{
            updateImg(nuevaImagen(imagen),user.getUid());
        }
    }

    private void updateImg(String s, String u){
        StorageReference usersRef = FirebaseStorage.getInstance().getReference().child(USERS+"/"+u+"/"+s);
        imgview.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable)imgview.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = usersRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(),"Error cargando imagen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getContext(),"Imagen Actualizada",Toast.LENGTH_SHORT).show();
                inctActual();
            }
        });
    }
}

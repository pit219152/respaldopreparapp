package com.icscrum.preparapp.DataBase;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.icscrum.preparapp.R;

public class ImagesLoader {
    private static final String IMAGENES = "imagenes";
    private static final String USUARIOS = "usuarios";
    private StorageReference storageReference;

    public ImagesLoader() {
    }
    public void mountImage(ImageView imageView, Context context, String file){
        storageReference = FirebaseStorage.getInstance().getReference().child(IMAGENES+"/"+file);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_defaul_dish);
        Glide.with(context).applyDefaultRequestOptions(requestOptions)
                .load(storageReference).into(imageView);

    }

    public void mountImageCache(ImageView imageView, Context context, String file){
        storageReference = FirebaseStorage.getInstance().getReference().child(IMAGENES+"/"+file);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_defaul_dish);
        Glide.with(context).applyDefaultRequestOptions(requestOptions)
                .load(storageReference)
                .onlyRetrieveFromCache(true)
                .into(imageView);

    }

    public void mountUserImage(ImageView imageView, Context context, String user, String image){
        storageReference = FirebaseStorage.getInstance().getReference().child(USUARIOS+"/"+user+"/"+image);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.default_user);
        Glide.with(context).applyDefaultRequestOptions(requestOptions)
                .load(storageReference)
                .onlyRetrieveFromCache(false)
                .into(imageView);

    }
}

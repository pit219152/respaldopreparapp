package com.icscrum.preparapp.DataBase;

import com.icscrum.preparapp.Models.Receta;

import java.util.ArrayList;

public class InnerFilter {
    String name , diners;
    ArrayList<String> categories, ingredients;

    public InnerFilter(String name, String diner, ArrayList<String> categories, ArrayList<String> ingredients) {
        this.name = name;
        this.diners = diner;
        this.categories = categories;
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiner() {
        return diners;
    }

    public void setDiner(String diner) {
        this.diners = diner;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<Receta> filterRecetas(ArrayList<Receta> recetas){
        ArrayList<Receta> filtered = new ArrayList<Receta>();
        if(categories.size()>0){
            if (ingredients.size()>0){
                if(!diners.isEmpty()){
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getComensales().equals(diners) &&
                                recetas.get(i).getCategorias().containsAll(categories) &&
                                recetas.get(i).getIngredientes().containsAll(ingredients)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }else{
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getCategorias().containsAll(categories) &&
                                recetas.get(i).getIngredientes().containsAll(ingredients)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }
            }else{
                if(!diners.isEmpty()){
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getComensales().equals(diners) &&
                                recetas.get(i).getCategorias().containsAll(categories)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }else{
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getCategorias().containsAll(categories)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }
            }
        }else{
            if (ingredients.size()>0){
                if(!diners.isEmpty()){
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getComensales().equals(diners) &&
                                recetas.get(i).getIngredientes().containsAll(ingredients)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }else{
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getIngredientes().containsAll(ingredients)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }
            }else{
                if(!diners.isEmpty()){
                    for(int i=0;i<recetas.size();i++){
                        if(recetas.get(i).getComensales().equals(diners)){
                            filtered.add(recetas.get(i));
                        }
                    }
                }else{
                    return recetas;
                }
            }
        }

        return filtered;
    }
}

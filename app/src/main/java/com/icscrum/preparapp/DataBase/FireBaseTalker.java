package com.icscrum.preparapp.DataBase;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.icscrum.preparapp.CatalogueFragment;
import com.icscrum.preparapp.CreateRecipeFragment;
import com.icscrum.preparapp.FiltredRecipesFragment;
import com.icscrum.preparapp.MainActivity;
import com.icscrum.preparapp.Models.Categorie;
import com.icscrum.preparapp.Models.Receta;
import com.icscrum.preparapp.RecipeInfoFragment;
import com.icscrum.preparapp.SearchFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FireBaseTalker {
    //Est clase se encargara de realaizar la comunicacion con la base de Datos en FireBase
    private FirebaseFirestore fDB = null;

    public static final String CATEGORIES = "categorias";
    public static final String INGREDIENTES = "ingredientes";
    public static final String RECETAS = "recetas";
    public static final String AUTOR = "autor";
    public static final String NOMBRE = "nombre";
    public static final String DESCRIPCION = "descripcion";
    public static final String COMENSALES = "comensales";
    public static final String CANTIDADES = "cantidades";
    public static final String IMAGEN = "imagen";
    public static final String HERMANA = "hermana";
    public static final String USERS = "usuarios";
    public static final String FUENTE = "fuente";
    public static final String NINGUNA = "Ninguna";
    public static final String TAG = "FIREBASE: ";
    public static final String NONE = "none";
    public static final String SAVED = "saved";
    public static final String CREATED = "created";
    public static final int LIMIT = 16;
    private String oLastOne="o",nLastOne="";
    private CatalogueFragment catalogueFragment;
    private SearchFragment searchFragment;
    private FiltredRecipesFragment filtredRecipesFragment;
    private RecipeInfoFragment recipeInfoFragment;
    private CreateRecipeFragment createRecipeFragment;
    private MainActivity mainActivity;

    public FireBaseTalker(){
        fDB = FirebaseFirestore.getInstance();
    }

    public void setCatalogueFragment(CatalogueFragment catalogueFragment) {
        this.catalogueFragment = catalogueFragment;
    }

    public void setCreateRecipeFragment(CreateRecipeFragment createRecipeFragment) {
        this.createRecipeFragment = createRecipeFragment;
    }

    public void setRecipeInfoFragment(RecipeInfoFragment recipeInfoFragment) {
        this.recipeInfoFragment = recipeInfoFragment;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void setSearchFragment(SearchFragment searchFragment) {
        this.searchFragment = searchFragment;
    }

    public void setFiltredRecipesFragment(FiltredRecipesFragment filtredRecipesFragment) {
        this.filtredRecipesFragment = filtredRecipesFragment;
    }

    public void loadCategories(){
        fDB.collection(CATEGORIES).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    ArrayList<Categorie> categories = new ArrayList<>();
                    for(DocumentSnapshot doc: task.getResult()){
                        String cat = (String) doc.get(NOMBRE);
                        categories.add(new Categorie(cat, false));
                    }
                    searchFragment.onLoadCategories(categories);
                }
            }
        });
    }

    public void loadCategoriesCreate(){

        fDB.collection(CATEGORIES).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    ArrayList<Categorie> categories = new ArrayList<>();
                    for(DocumentSnapshot doc: task.getResult()){
                        String cat = (String) doc.get(NOMBRE);
                        categories.add(new Categorie(cat, false));
                    }
                    createRecipeFragment.onCategoriesLoad(categories);
                }
            }
        });
    }

    public void loadIngredientes(){
        final ArrayList<Categorie> categories = new ArrayList<>();
        fDB.collection(INGREDIENTES).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot doc: task.getResult()){
                        String cat = (String) doc.get(NOMBRE);
                        categories.add(new Categorie(cat, false));
                    }
                    searchFragment.onLoadIngredients(categories);
                }
            }
        });
    }

    public void loadMyRecipes(String owner){
        fDB.collection(USERS).document(owner).collection(SAVED).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    ArrayList<Receta> misrecetas = new ArrayList<>();
                    for(DocumentSnapshot doc: task.getResult()){
                        Receta r = doc.toObject(Receta.class);
                        r.setId(doc.getId());
                        misrecetas.add(r);
                    }
                    mainActivity.onLoadMyRecipes(misrecetas);
                }else{
                    mainActivity.onLoadMyRecipes(null);
                }
            }
        });
    }

    public void loadCreatedRecipes(String owner){
        fDB.collection(USERS).document(owner).collection(CREATED).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    ArrayList<Receta> misrecetas = new ArrayList<>();
                    for(DocumentSnapshot doc: task.getResult()){
                        Receta r = doc.toObject(Receta.class);
                        r.setId(doc.getId());
                        misrecetas.add(r);
                    }
                    mainActivity.onLoadCreatedRecipes(misrecetas);
                }else{
                    mainActivity.onLoadCreatedRecipes(null);
                }
            }
        });
    }

    public void loadRecetas(){
        nLastOne = "";
        Query query=fDB.collection(RECETAS).orderBy(NOMBRE).limit(LIMIT);
        loadFromFirebase(query);
    }

    public void loadNextRecetas(){
        Query query=fDB.collection(RECETAS).orderBy(NOMBRE).startAt(nLastOne).limit(LIMIT);
        loadFromFirebase(query);
    }

    public void loadRecetasFiltred(String name,ArrayList<String> categories){
        char c = name.charAt(0);
        c++;
        String last = String.valueOf(c);
       Query query= fDB.collection(RECETAS).whereGreaterThanOrEqualTo(NOMBRE,name).whereLessThan(NOMBRE,last)
               .whereArrayContainsAny(CATEGORIES,categories);
       loadAll(query);
    }
    public void loadRecetasFiltred(String name){
        char c = name.charAt(0);
        c++;
        String last = String.valueOf(c);
        Query query= fDB.collection(RECETAS).whereGreaterThanOrEqualTo(NOMBRE,name).whereLessThan(NOMBRE,last);
        loadAll(query);
    }

    public void loadRecetasFiltred(){
        Query query= fDB.collection(RECETAS);
        loadAll(query);
    }

    public void loadRecetasFiltred(ArrayList<String> categories){
        Query query= fDB.collection(RECETAS).whereArrayContainsAny(CATEGORIES,categories);
        loadAll(query);
    }



    public void loadNextRecetasFiltred(ArrayList<String> c){
            Query query = fDB.collection(RECETAS).orderBy(NOMBRE).whereArrayContains(CATEGORIES, c).startAt(nLastOne).limit(LIMIT);
            loadFromFirebase(query);
    }

    private void loadFromFirebase(Query query){
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    ArrayList<Receta> recetas = new ArrayList<>();
                    for(QueryDocumentSnapshot document : task.getResult()) {
                        Receta r = document.toObject(Receta.class);
                        r.setId(document.getId());
                        recetas.add(r);
                    }
                    if(!nLastOne.equals(NONE) && recetas.size()>0 ){
                        nLastOne = recetas.get(recetas.size()-1).getNombre();
                        if(!nLastOne.equals(oLastOne)){
                            oLastOne=nLastOne;
                            recetas.remove(recetas.size()-1);
                            catalogueFragment.refreshCatalogue(recetas);
                        }else{
                            catalogueFragment.refreshCatalogue(recetas);
                            nLastOne = NONE;
                        }
                    }

                }else{
                    Log.w(TAG, "Error cargando Categorias",task.getException());
                }
            }
        });
    }

    private void loadAll(Query query){
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                ArrayList<Receta> recetas = new ArrayList<>();
                for(QueryDocumentSnapshot document : task.getResult()) {
                    Receta r = document.toObject(Receta.class);
                    r.setId(document.getId());
                    recetas.add(r);
                }
                filtredRecipesFragment.onLoadFiltredRecipes(recetas);
            }
        });
    }

    public void uploadMyRecipe(Receta receta, String owner){
        Map<String, Object> dataDoc = new HashMap<>();
        dataDoc.put(NOMBRE, receta.getNombre());
        dataDoc.put(DESCRIPCION,receta.getDescripcion());
        dataDoc.put(COMENSALES,receta.getComensales());
        dataDoc.put(CATEGORIES,receta.getCategorias());
        dataDoc.put(INGREDIENTES,receta.getIngredientes());
        dataDoc.put(CANTIDADES,receta.getCantidades());
        dataDoc.put(IMAGEN,receta.getImagen());
        dataDoc.put(AUTOR,receta.getAutor());
        dataDoc.put(FUENTE,receta.getFuente());
        fDB.collection(USERS).document(owner).collection(SAVED).add(dataDoc).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                recipeInfoFragment.confirmSaved(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                recipeInfoFragment.confirmSaved(false);
            }
        });
    }

    public void uploadCreatedRecipe(Receta receta, String owner){
        Map<String, Object> dataDoc = new HashMap<>();
        dataDoc.put(NOMBRE, receta.getNombre());
        dataDoc.put(DESCRIPCION,receta.getDescripcion());
        dataDoc.put(COMENSALES,receta.getComensales());
        dataDoc.put(CATEGORIES,receta.getCategorias());
        dataDoc.put(INGREDIENTES,receta.getIngredientes());
        dataDoc.put(CANTIDADES,receta.getCantidades());
        dataDoc.put(IMAGEN,receta.getImagen());
        dataDoc.put(AUTOR,receta.getAutor());
        dataDoc.put(HERMANA,receta.getHermana());
        if(receta.getFuente().isEmpty()){
            dataDoc.put(FUENTE,NINGUNA);
        }else{
            dataDoc.put(FUENTE,receta.getFuente());
        }
        fDB.collection(USERS).document(owner).collection(CREATED).add(dataDoc).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                createRecipeFragment.confirmSaved(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                createRecipeFragment.confirmSaved(false);
            }
        });
    }

    public void uploadHermana(Receta receta, String owner){
        Map<String, Object> dataDoc = new HashMap<>();
        dataDoc.put(NOMBRE, receta.getNombre());
        dataDoc.put(DESCRIPCION,receta.getDescripcion());
        dataDoc.put(COMENSALES,receta.getComensales());
        dataDoc.put(CATEGORIES,receta.getCategorias());
        dataDoc.put(INGREDIENTES,receta.getIngredientes());
        dataDoc.put(CANTIDADES,receta.getCantidades());
        dataDoc.put(IMAGEN,receta.getImagen());
        dataDoc.put(AUTOR,receta.getAutor());
        if(receta.getFuente().isEmpty()){
            dataDoc.put(FUENTE,NINGUNA);
        }else{
            dataDoc.put(FUENTE,receta.getFuente());
        }
        fDB.collection(RECETAS).add(dataDoc).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                createRecipeFragment.confirmHermana(documentReference.getId());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                createRecipeFragment.confirmHermana("");
            }
        });
    }



    public void deleteSavedRecipe(String receta, String owner){
        fDB.collection(USERS).document(owner).collection(SAVED).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        recipeInfoFragment.confirmSaved(true);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        recipeInfoFragment.confirmSaved(false);
                    }
        });
    }

    public void deleteCreatedRecipe(String receta, String owner){
        fDB.collection(USERS).document(owner).collection(CREATED).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        recipeInfoFragment.confirmDel(true);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                recipeInfoFragment.confirmDel(false);
            }
        });
    }

    public void deleteHermanaRecipe(String receta){
        fDB.collection(RECETAS).document(receta).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        recipeInfoFragment.confirmDel(true);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                recipeInfoFragment.confirmDel(false);
            }
        });
    }
}
